<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a PyroGPG module for PyroCMS
 *
 * @author 	Nanohard
 * @website	http://nanohard.net
 * @package 	PyroCMS
 * @subpackage 	PyroGPG Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'items';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->load->model('pyrogpg_m');
		$this->load->library('form_validation');
		$this->lang->load('pyrogpg');

		// Set the validation rules
		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|max_length[100]|required'
			)
		);

		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')
						->append_css('module::admin.css');
	}

	/**
	 * List all items
	 */
	public function index()
	{
		// here we use MY_Model's get_all() method to fetch everything
		$items = $this->pyrogpg_m->get_log();
                //print_r($items); //debugging

/*		// Build the view with sample/views/admin/items.php
		$this->template
			->title($this->module_details['name'])
			->set('items', $items)
			->build('admin/items');
*/

            $this->load->view('admin/index');

	}

	public function create()
	{
		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);

		// check if the form validation passed
		if ($this->form_validation->run())
		{
			// See if the model can create the record
			if ($this->pyrogpg_m->create($this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('pyrogpg.success'));
				redirect('admin/pyrogpg');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('pyrogpg.error'));
				redirect('admin/pyrogpg/create');
			}
		}
		
		$pyrogpg = new stdClass;
		foreach ($this->item_validation_rules as $rule)
		{
			$pyrogpg->{$rule['field']} = $this->input->post($rule['field']);
		}

		// Build the view using pyrogpg/views/admin/form.php
		$this->template
			->title($this->module_details['name'], lang('pyrogpg.new_item'))
			->set('pyrogpg', $pyrogpg)
			->build('admin/form');
	}
	
	public function edit($id = 0)
	{
		$pyrogpg = $this->pyrogpg_m->get($id);

		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);

		// check if the form validation passed
		if ($this->form_validation->run())
		{
			// get rid of the btnAction item that tells us which button was clicked.
			// If we don't unset it MY_Model will try to insert it
			unset($_POST['btnAction']);
			
			// See if the model can create the record
			if ($this->pyrogpg_m->update($id, $this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('pyrogpg.success'));
				redirect('admin/pyrogpg');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('pyrogpg.error'));
				redirect('admin/pyrogpg/create');
			}
		}

		// Build the view using pyrogpg/views/admin/form.php
		$this->template
			->title($this->module_details['name'], lang('pyrogpg.edit'))
			->set('pyrogpg', $pyrogpg)
			->build('admin/form');
	}
	
	public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			// pass the ids and let MY_Model delete the items
			$this->pyrogpg_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			// they just clicked the link so we'll delete that one
			$this->pyrogpg_m->delete($id);
		}
		redirect('admin/pyrogpg');
	}
}
