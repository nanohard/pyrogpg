# PyroGPG
### Developed for PyroCMS 2.2 Community Edition

## Installation
### Requirements
- Linux server
- GPGme installed in PHP

### How-To
If you have a Linux server but not GPGme, follow these instructions

(you may have to add "sudo" to the beginning of each line):

`apt-get install php5-dev gpgsm gnupg libgpg-error-dev libassuan-dev php-pear`

`pecl install gnupg`

Add this to your php.ini file:

`extension=gnupg.so`

## What is PyroGPG?
A module for PyroCMS to replace the contact form.

It will encrypt the message body using GPG before it's stored in the database and sent to email.

### Currently
- It does not encrypt attachments (to-do)