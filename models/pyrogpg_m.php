<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PyroGPG_m extends MY_Model {

public function get_log()
	{
		return $this->db
			->get('pyrogpg_log')
			->result();
	}
	
	public function insert_log($input)
	{		
		return $this->db->insert('pyrogpg_log', array(
			'email'			=> isset($input['email']) ? $input['email'] : '',
			'subject' 		=> substr($input['subject'], 0, 255),
                        'fingerprint'           => $input['fingerprint'],
			'message' 		=> $input['body'],
			'sender_agent' 	=> $input['sender_agent'],
			'sender_ip' 	=> $input['sender_ip'],
			'sender_os' 	=> $input['sender_os'],	
			'sent_at' 		=> time(),
			'attachments'	=> isset($input['attach']) ? implode('|', $input['attach']) : '',
		));
	}
}