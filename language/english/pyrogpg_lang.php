<?php
//messages
$lang['pyrogpg:success']		=	'It worked';
$lang['pyrogpg:error']			=	'It didn\'t work';
$lang['pyrogpg:no_items']		=	'No Items';

//page titles
$lang['pyrogpg:create']			=	'Create Item';

//labels
$lang['pyrogpg:name']			=	'Name';
$lang['pyrogpg:slug']			=	'Slug';
$lang['pyrogpg:manage']			=	'Manage';
$lang['pyrogpg:item_list']		=	'Item List';
$lang['pyrogpg:view']			=	'View';
$lang['pyrogpg:edit']			=	'Edit';
$lang['pyrogpg:delete']			=	'Delete';

//buttons
$lang['pyrogpg:custom_button']	=	'Custom Button';
$lang['pyrogpg:items']			=	'Items';
?>