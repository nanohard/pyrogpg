<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @author 	Nanohard
 * @website	http://nanohard.net
 * @package 	PyroCMS
 * @subpackage 	PyroGPG Module
 */
class Module_PyroGPG extends Module {

	public $version = '1.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'PyroGPG'
			),
			'description' => array(
				'en' => 'Used to encrypt email messages to the website owner.'
			),
			'frontend' => false,
			'backend' => true,
			'menu' => 'data',
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('pyrogpg_log');

		$tables = array(
			'pyrogpg_log' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true,),
				'email' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
				'subject' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
				'fingerprint' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
                                'message' => array('type' => 'TEXT',),
				'sender_agent' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
				'sender_ip' => array('type' => 'VARCHAR', 'constraint' => 45, 'default' => '',),
				'sender_os' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
				'sent_at' => array('type' => 'INT', 'constraint' => 11, 'default' => 0,),
				'attachments' => array('type' => 'TEXT',),
			)
		);

		if ( ! $this->install_tables($tables))
		{
			return false;
		}

            $this->load->driver('streams');
            $this->streams->streams->add_stream('PyroGPG Settings', 'settings', 'pyrogpg_settings', 'pyrogpg_');

            $fields = array(
        array(
            'name'          => 'GPG Path',
            'slug'          => 'gpg_path',
            'namespace'     => 'pyrogpg_settings',
            'type'          => 'text',
            //'extra'         => array('max_length' => 255),
            'assign'        => 'settings',
            'title_column'  => true,
            'required'      => false,
            'unique'        => false
        ),
        array(
            'name'          => 'Fingerprint',
            'slug'          => 'fingerprint',
            'namespace'     => 'pyrogpg_settings',
            'type'          => 'text',
            //'extra'         => array('max_length' => 255),
            'assign'        => 'settings',
            'title_column'  => true,
            'required'      => false,
            'unique'        => false
        )
/*        array(
            'name'          => 'Password',
            'slug'          => 'password',
            'namespace'     => 'pyrogpg_settings',
            'type'          => 'encrypt',
            //'extra'         => array('max_length' => 255),
            'assign'        => 'settings',
            'title_column'  => true,
            'required'      => false,
            'unique'        => false
        ),

        array(
            'name'          => 'Login Successful',
            'slug'          => 'success',
            'namespace'     => 'pyrogpg_settings',
            'type'          => 'text',
            //'extra'         => array('max_length' => 255),
            'assign'        => 'settings',
            'title_column'  => true,
            'required'      => false,
            'unique'        => false
        ),
        array(
            'name'          => 'Date',
            'slug'          => 'date',
            'namespace'     => 'pyrogpg_settings',
            'type'          => 'datetime',
            'extra'         => array('use_time' => 'yes', 'storage' => 'datetime'),
            'assign'        => 'settings',
            'title_column'  => true,
            'required'      => false,
            'unique'        => false
        )
*/
        );
 
        $this->streams->fields->add_fields($fields);
    

        return true;
    }         
                
                


	public function uninstall() {

            $this->load->driver('streams');
            $this->streams->streams->delete_stream('settings', 'pyrogpg_settings');
            $this->streams->utilities->remove_namespace('pyrogpg_settings');
            $this->streams->fields->delete_field('gpg_path', 'pyrogpg_settings');
            $this->streams->fields->delete_field('fingerprint', 'pyrogpg_settings');
            
            $this->dbforge->drop_table('pyrogpg_log');
            $this->db->delete('settings', array('module' => 'pyrogpg'));
	    return true;
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
