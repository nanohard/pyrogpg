<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php
$this->load->driver('streams');

$params = array(
    'stream'    => 'settings',
    'namespace' => 'pyrogpg_settings',
);
$finger = $this->streams->entries->get_entries($params);

if (isset($finger['entries'][0])) {
    $id = $finger['entries'][0]['id'];
}

if (isset($finger['entries'][0])) {
    $current_gpg_path = $finger['entries'][0]['gpg_path'];
    } else {
    $current_gpg_path = "Not yet set";
}

if (isset($finger['entries'][0])) {
    $current_fingerprint = $finger['entries'][0]['fingerprint'];
    } else {
    $current_fingerprint = "Not yet set";
}
//print_r($finger); //for debugging
?>

<?php //if the form is being submitted, do this:
    if ($this->input->post() == true) {
        $entry_data1 = array(
            'gpg_path'    => $this->input->post('gpg_path'),
            'fingerprint' => $this->input->post('fingerprint')
        );
        
        $entry_data2 = array(
            'gpg_path'    => $this->input->post('gpg_path'),
            'fingerprint' => $this->input->post('fingerprint')
        );
        

        if (!isset($finger['entries'][0])) {
            $this->streams->entries->insert_entry($entry_data1, 'settings', 'pyrogpg_settings');
        } else {
            $this->streams->entries->update_entry($id, $entry_data2, 'settings', 'pyrogpg_settings');
            //$this->streams->entries->insert_entry($entry_data, 'settings', 'pyrogpg_settings'); // working
        }
        header("location: " . $_SERVER['REQUEST_URI']);
    }
?>

<section class="title">
    <h2>PyroGPG</h2>
</section>

<?php //putenv('GNUPGHOME=/home/pyro/public_html/addons/shared_addons/modules/pyroGPG/.gnupg');

?>

<section class="item">
    <section class="content">
        <h4>Your current Fingerprint is: <?php echo $current_fingerprint ?>
        </h4>
    </section>
    
    <section class="content">
        <h4>Your current GPG Path is: <?php echo $current_gpg_path ?>
        </h4>
    </section>
</section>
<br>
    
<section class="item">
    <section class="content">
<h3>Instructions:</h3>
<br>

<h4>To generate key:</h4>
<p>Open up your command line/terminal.<p>
<p>Type the following:</p>
<code>gpg --gen-key</code>
<p>Follow the instructions GPG gives you.
<br>When you're done, it will show you the "fingerprint" that you need to enter here.</p>

<br>
<h4>To find your GPG path:</h4>
<p>In your command line, type:</p>
<code>locate .gnupg</code>
<p>Include the entire path. It should begin with a forward slash "/" and end with ".gnupg"
<br>It's usually something like: /home/user/.gnupg</p>
    </section>
</section>

<?php echo form_open(uri_string(), 'class="crud"') ?>
<div class="form_inputs">
<ul>
    <li>
        <label for="fingerprint">Enter you public key fingerprint:</label>
        <div class="input"><?php echo form_input('fingerprint');?></div>
    </li>
<br>
    <li>
        <label for="gpg_path">Enter the path to your GPG folder:</label>
        <div class="input"><?php echo form_input('gpg_path');?></div>
    </li>
</ul>

<div class="buttons">
<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
</div><!-- /.buttons -->

<?php echo form_close();?>
<!--</form>-->